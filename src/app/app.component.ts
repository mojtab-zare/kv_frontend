import {Component, OnInit} from '@angular/core';
import {TokenService} from "./user/token.service";
import {UserService} from "./user/user.service";
import {MdSnackBar} from "@angular/material";
import {LoaderService} from "./loader/loader.service";
import {LoaderState} from "./loader/loader-state";
import {BookmarkService} from "./bookmark/bookmark.service";
import {TabsService} from "./tabs/tabs.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public itemList="bookmark";

  constructor(public snackBar: MdSnackBar,
              public userService: UserService,
              public bookmarkService:BookmarkService,
              public tabsService:TabsService,
              public tokenService: TokenService) {

  }

  ngOnInit(): void {
    this.tokenService.readToken();
    if (this.tokenService.loggedIn) {
      this.userService.initApp();
    }

    this.userService.userCreated.subscribe(
      () => {
        this.snackBar.open("User Created!", null, {
          duration: 4000,
        });
      }
    );
  }

  onChangeList(change:string){
    this.itemList = change;
    switch (change){
      case "bookmark":{
        this.bookmarkService.getBookmarks();
        break;
      }
      case 'tab':{
        this.tabsService.getTabs();
        break;
      }
    }

  }

}
