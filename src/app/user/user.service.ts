import {EventEmitter, Injectable} from '@angular/core';
import 'rxjs/Rx';
import {OauthToken} from "./oauth-token";
import {KeyvalueHttpService} from "../http/keyvalue-http.service";
import {ValidationErrors} from "@angular/forms";
import {Observable} from "rxjs/Observable";
import {TokenService} from "./token.service";
import {UserDetails} from "./user-details";
import {BookmarkService} from "../bookmark/bookmark.service";
import {ResponseBody} from "../http/response-body";

@Injectable()
export class UserService {


  // public isLoggedIn: boolean = false;
  public userDetails = new EventEmitter<UserDetails>();
  public userCreated = new EventEmitter();

  constructor(private httpService: KeyvalueHttpService,
              private tokenService: TokenService,
              private bookmarkService: BookmarkService) {
  }


  login(email: string, password: string, remember: boolean) {
    let observable = this.httpService.login(email, password);
    observable.subscribe(
      (data: OauthToken) => {
        // this.loaderService.hide();
        this.tokenService.oauthToken = data;
        if (remember) {
          this.tokenService.saveToken();
        }
        this.initApp();
      },
      (error) => {
        console.error(error);
        // this.loaderService.hide();
      }
    );

  }


  signUp(email: string, password: string) {
    let observable = this.httpService.signUp(email, password);
    observable.subscribe(
      (data: ResponseBody) => {
        if (data.status === 'success') {
          this.userCreated.emit();
        } else {
          console.debug(data)
        }
      },
      error => {
        console.error(error);
      }
    );
  }


  validateEmail(email: string): Observable<ValidationErrors | null> {
    return this.httpService.checkEmailValidation(email).map(value => {
      if (value.json() == null) {
        // console.log("null Valid");
        return null;
      }
      console.log(value.json());
      return value.json();
    });
  }


  getUserDetails() {
    this.httpService.userDetails().subscribe(
      (user: UserDetails) => {
        this.userDetails.emit(user);
      }
    );
  }

  initApp() {
    this.getUserDetails();
    this.bookmarkService.getBookmarks();

  }


}
