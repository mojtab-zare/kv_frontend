import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from "./user.service";

// const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const EMAIL_REGEX = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;


@Component({
  selector: 'kvfe-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  loginForm: FormGroup;
  signupForm: FormGroup;

  loginEmail: FormControl;
  loginPassword: FormControl;
  loginRememberMe:FormControl;

  signUpEmail: FormControl;
  signUpPassword: FormControl;
  signUpConfirmPassword: FormControl;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.loginEmail = new FormControl('', Validators.required);
    this.loginPassword = new FormControl('', Validators.required);
    this.loginRememberMe = new FormControl();
    this.loginForm = new FormGroup({
      'email': this.loginEmail,
      'password': this.loginPassword,
      'remember_me':this.loginRememberMe,
    });

    this.signUpEmail = new FormControl('', [Validators.required, Validators.pattern(EMAIL_REGEX)], this.validateEmail(this.userService));
    //todo add min length and max length limit.
    this.signUpPassword = new FormControl('', Validators.required);
    this.signUpConfirmPassword = new FormControl('',
      [Validators.required, this.confirmPasswordValidator(this.signUpPassword)]);
    this.signupForm = new FormGroup({
      'email': this.signUpEmail,
      'password': this.signUpPassword,
      'confirmPassword': this.signUpConfirmPassword,

    });

  }

  onLogin() {
    console.log("attempt to login.");
    let email = this.loginEmail.value;
    let password = this.loginPassword.value;
    let remember = this.loginRememberMe.value;
    this.userService.login(email, password,remember);
  }

  onSignUp() {
    let email = this.signUpEmail.value;
    let password = this.signUpPassword.value;
    this.userService.signUp(email, password);
  }

  validateEmail(userService: UserService) {
    return (emailCtrl: FormControl) => {
      let value = emailCtrl.value;
      return userService.validateEmail(value);
    };
  }

  confirmPasswordValidator(passwordCtrl: FormControl) {
    return (confirmPasswordCtrl: FormControl) => {
      if (confirmPasswordCtrl.value == passwordCtrl.value) {
        return null;
      } else {
        return {'passwordsMatch': true};
      }
    };
  }


}
