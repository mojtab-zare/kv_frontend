import {OauthToken} from "./oauth-token";
import {EventEmitter} from "@angular/core";

export class TokenService {
  static readonly TOKEN_KEY: string = "kv-token";

  // logoutEvent = new EventEmitter();

  constructor() {
  }

  get loggedIn(): boolean {
    return this.oauthToken != null;
  }


  private _oauthToken: OauthToken;


  get oauthToken(): OauthToken {
    return this._oauthToken;
  }

  set oauthToken(value: OauthToken) {
    this._oauthToken = value;
  }

  get tokenType(): string {
    if (this.oauthToken == null) {
      return null;
    } else {
      return this.oauthToken.token_type;
    }
  }


  get accessToken(): string {
    if (this.oauthToken == null) {
      return null;
    } else {
      return this.oauthToken.access_token;
    }
  }

  get userId(): string {
    if (this.oauthToken == null) {
      return null;
    } else {
      return this.oauthToken.user_id;
    }
  }

  public logout() {
    this.oauthToken = null;
    this.removeToken();
    // this.logoutEvent.emit({});
    // console.debug("emitted")
  }

  removeToken() {
    localStorage.removeItem(TokenService.TOKEN_KEY);
  }


  saveToken() {
    localStorage.setItem(TokenService.TOKEN_KEY, JSON.stringify(this.oauthToken));
  }

  readToken() {
    this.oauthToken = JSON.parse(localStorage.getItem(TokenService.TOKEN_KEY));
  }

}
