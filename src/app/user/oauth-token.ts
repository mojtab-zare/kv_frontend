export class OauthToken {

  constructor(public access_token: string,
              public expires_in: number,
              public jti: string,
              public scope: string,
              public token_type: string,
              public user_id: string) {
  }

}
