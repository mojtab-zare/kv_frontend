import {EventEmitter, Injectable} from '@angular/core';
import {Tabs} from "./tab";
import {KeyvalueHttpService} from "../http/keyvalue-http.service";

@Injectable()
export class TabsService {

  public tabsChanged = new EventEmitter<Tabs>();


  constructor(private httpService: KeyvalueHttpService) {
  }


  getTabs() {
    this.httpService.getTabs().subscribe(
      (tabs)=>{
        this.tabsChanged.emit(tabs);
        console.log(tabs);
      }
    );
  }


}
