import {Component, Input} from '@angular/core';
import {Tab} from "./tab";

@Component({
  selector: 'kvfe-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.css']
})
export class TabComponent {

  @Input() tab: Tab;

  constructor() {
  }

}
