import {Component, OnInit} from '@angular/core';
import {Tabs} from "./tab";
import {TabsService} from "./tabs.service";

@Component({
  selector: 'kvfe-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit {

  public tabsWrapper: Tabs;

  constructor(public tabsService: TabsService) {
    this.tabsWrapper = new Tabs("", []);
  }

  ngOnInit() {
    this.tabsService.getTabs();
    this.tabsService.tabsChanged.subscribe(
      (tabs) => this.tabsWrapper = tabs
    );
  }

}
