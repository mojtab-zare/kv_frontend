export class Tab {
  constructor(public title: string, public  url: string, public favIconUrl: string) {
  }
}


export class Tabs {
  constructor(public _id: string, public tabs: Tab[]) {
  }

}
