import {Injectable} from '@angular/core';
import {OauthToken} from "../user/oauth-token";
import {Headers, Http, RequestOptions, Response, URLSearchParams} from "@angular/http";
import {Bookmark} from "../bookmark/bookmark";
import {TokenService} from "../user/token.service";
import {UserDetails} from "../user/user-details";
import {ResponseBody} from "./response-body";
import {HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {LoaderService} from "../loader/loader.service";
import {Tabs} from "../tabs/tab";

@Injectable()
export class KeyvalueHttpService {

  static readonly BASE_URL: string = "http://127.0.0.1:8080/";
  static readonly LOGIN_URL: string = "oauth/token";
  static readonly SIGN_UP_URL: string = "sign-up";
  static readonly BOOKMARK_URL: string = "bookmark";
  static readonly TAB_URL: string = "tab";
  static readonly EMAIL_URL: string = "email";
  static readonly USER_DETAILS: string = "user";

  // public oauthToken: OauthToken;


  constructor(private http: Http,
              private tokenService: TokenService,
              private loaderService: LoaderService) {
  }

  login(email: string, password: string) {
    let headers = new Headers();
    headers.append("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
    headers.append('Authorization', 'Basic a2V5dmFsdWU6cmk4amJwNjFCZG8ydHhadEhDWWtUb1J5');
    // let data = new URLSearchParams();
    // data.append("grant_type", "password");
    // data.append("username", email);
    // data.append("password", password);
    let body = `grant_type=password&username=${email}&password=${password}`;
    let options = new RequestOptions({
      method: "POST",
      headers: headers
    });
    // return this.http.request(UserService.BASE_URL + UserService.LOGIN_URL, options);
    return this.http.post(KeyvalueHttpService.BASE_URL + KeyvalueHttpService.LOGIN_URL, body, options)
      .map(value => value.json()as OauthToken);
  }


  signUp(email: string, password: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let options = new RequestOptions({
      headers: headers
    });

    let body = {
      'email': email,
      'password': password
    };
    return this.http.post(KeyvalueHttpService.BASE_URL + KeyvalueHttpService.SIGN_UP_URL, body, options)
      .map(value => value.json()as ResponseBody);
  }


  loadBookmarks() {
    let headers = new Headers();
    this.addAuthorizationHeader(headers);
    let options = new RequestOptions({
      headers: headers
    });
    this.loaderService.show();
    return this.http.get(KeyvalueHttpService.BASE_URL + KeyvalueHttpService.BOOKMARK_URL, options)
      .catch(this.errorHandler)
      .finally(() => this.loaderService.hide())
      .map((value) => value.json().data as Bookmark[]);
  }

  checkEmailValidation(email: string) {
    const url = KeyvalueHttpService.BASE_URL + KeyvalueHttpService.EMAIL_URL;
    let data = new URLSearchParams();
    data.append("email", email);
    let options = new RequestOptions({
      search: data
    });
    return this.http.get(url, options);
  }


  userDetails() {
    const url = KeyvalueHttpService.BASE_URL + KeyvalueHttpService.USER_DETAILS;
    let headers = new Headers();
    this.addAuthorizationHeader(headers);
    let options = new RequestOptions({
      headers: headers
    });
    return this.http.get(url, options).map(value => value.json() as UserDetails);
  }

  updateBookmark(bookmark: Bookmark) {
    const url = KeyvalueHttpService.BASE_URL + KeyvalueHttpService.BOOKMARK_URL +
      '/' + bookmark._id;
    let headers = new Headers();
    this.addAuthorizationHeader(headers);
    let options = new RequestOptions({
      headers: headers
    });
    this.loaderService.show();
    return this.http.put(url, bookmark, options)
      .catch(this.errorHandler)
      .finally(() => this.loaderService.hide())
      .map(value => value.json() as ResponseBody
      );
  }

  getTabs() {
    let headers = new Headers();
    this.addAuthorizationHeader(headers);
    let options = new RequestOptions({
      headers: headers
    });
    this.loaderService.show();
    return this.http.get(KeyvalueHttpService.BASE_URL + KeyvalueHttpService.TAB_URL, options)
      .catch(this.errorHandler)
      .finally(() => this.loaderService.hide())
      .map((value) => {
        let tabs = value.json() as Tabs;
        console.debug(tabs);
        return tabs;
      });
  }

  private addAuthorizationHeader(headers: Headers) {
    headers.append('Authorization', `${this.tokenService.tokenType} ${this.tokenService.accessToken}`);
  }

  private errorHandler(err: HttpErrorResponse): Observable<Response> {
    if (err.error instanceof Error) {
      // A client-side or network error occurred. Handle it accordingly.
      console.log('An error occurred:', err.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
    }
    return Observable.empty<Response>();
  }


}
