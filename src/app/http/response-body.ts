export class ResponseBody {

  static readonly SUCCESS: string = "success";
  static readonly FAILED: string = "failed";

  constructor(public status: string, public data: any){}
}
