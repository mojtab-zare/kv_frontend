import { Component, OnInit } from '@angular/core';
import {TokenService} from "./user/token.service";
import {UserService} from "./user/user.service";

@Component({
  selector: 'kvfe-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  public userEmail: string = '';

  constructor(public tokenService:TokenService,private userService:UserService) { }

  ngOnInit() {
    this.userService.userDetails.subscribe(
      user => this.userEmail = user.email
    );
  }


  onLogout(){
    this.tokenService.logout();
  }


}
