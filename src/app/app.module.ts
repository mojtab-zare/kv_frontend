import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';

import {AppComponent} from './app.component';
import {
  MdButtonModule,
  MdCheckboxModule,
  MdChipsModule,
  MdDialogModule,
  MdFormFieldModule,
  MdGridListModule,
  MdIconModule,
  MdInputModule,
  MdListModule,
  MdMenuModule,
  MdProgressBarModule,
  MdSnackBarModule,
  MdTabsModule,
  MdToolbarModule
} from "@angular/material";
import {HttpModule} from "@angular/http";
import {ReactiveFormsModule} from "@angular/forms";
import {HeaderComponent} from './header.component';
import {BookmarkComponent} from './bookmark/bookmark.component';
import {BookmarkListComponent} from './bookmark/bookmark-list.component';
import {LoginComponent} from './user/login.component';
import {UserService} from "./user/user.service";
import {KeyvalueHttpService} from "./http/keyvalue-http.service";
import {BookmarkService} from "./bookmark/bookmark.service";
import {TokenService} from "./user/token.service";
import {LoaderService} from "./loader/loader.service";
import {EditBookmarkDialogComponent} from './bookmark/edit-bookmark-dialog.component';
import { LoaderComponent } from './loader/loader.component';
import { TabsComponent } from './tabs/tabs.component';
import { TabComponent } from './tabs/tab.component';
import {TabsService} from "./tabs/tabs.service";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BookmarkComponent,
    BookmarkListComponent,
    LoginComponent,
    EditBookmarkDialogComponent,
    LoaderComponent,
    TabsComponent,
    TabComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    // FormsModule,
    ReactiveFormsModule,
    HttpModule,
    FlexLayoutModule,
    MdButtonModule,
    MdToolbarModule,
    MdIconModule,
    MdMenuModule,
    // MdSidenavModule,
    MdCheckboxModule,
    MdSnackBarModule,
    MdProgressBarModule,
    MdDialogModule,
    MdListModule,
    MdTabsModule,
    MdGridListModule,
    MdInputModule,
    MdFormFieldModule,
    MdChipsModule
  ],
  providers: [
    UserService,
    BookmarkService,
    KeyvalueHttpService,
    TokenService,
    LoaderService,
    TabsService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    EditBookmarkDialogComponent
  ]

})
export class AppModule {
}
