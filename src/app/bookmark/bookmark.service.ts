import {EventEmitter, Injectable} from '@angular/core';
import {KeyvalueHttpService} from "../http/keyvalue-http.service";
import {Bookmark} from "./bookmark";
import {ResponseBody} from "../http/response-body";

@Injectable()
export class BookmarkService {
  public bookmarksChanged = new EventEmitter<Bookmark[]>();
  private bookmarks: Bookmark[];

  // private shouldReload = true;

  constructor(private httpService: KeyvalueHttpService) {
  }

  getBookmarks() {

    // if (this.shouldReload) {
    this.httpService.loadBookmarks().subscribe(
      (data: Bookmark[]) => {
        this.bookmarksChanged.emit(data);
        this.bookmarks = data;
      },
      error => {
        console.error(error);
      }
    );
    console.log("read bookmarks");
    // this.shouldReload = false;
    // }
  }


  updateBookmark(bookmark: Bookmark) {


    let find = this.bookmarks.find(item => {
      return bookmark._id === item._id;
    });

    if (!(find.url === bookmark.url && find.title === bookmark.title)) {
      this.httpService.updateBookmark(bookmark).subscribe(
        (result: ResponseBody) => {
          if (result.status === ResponseBody.SUCCESS) {
            find.url = bookmark.url;
            find.title = bookmark.title;
          }
        }
      );
    }


  }


}
