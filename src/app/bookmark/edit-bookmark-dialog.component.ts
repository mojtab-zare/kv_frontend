import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from "@angular/material";
import {FormControl, FormGroup} from "@angular/forms";
import {Bookmark} from "./bookmark";

@Component({
  selector: 'kvfe-edit-bookmark-dialog',
  templateUrl: 'edit-bookmark-dialog.component.html',
  styles: []
})

export class EditBookmarkDialogComponent implements OnInit {

  editForm: FormGroup;
  titleInput: FormControl;
  urlInput: FormControl;
  id: string;
  constructor(public dialogRef: MdDialogRef<EditBookmarkDialogComponent>,
              @Inject(MD_DIALOG_DATA) public data: Bookmark,) {
    this.id = data._id;
  }

  ngOnInit() {

    this.titleInput = new FormControl(this.data.title);
    this.urlInput = new FormControl(this.data.url);

    this.editForm = new FormGroup({
      'title': this.titleInput,
      'url': this.urlInput,
    });

  }

  // onEdit() {
  //
  // }
}
