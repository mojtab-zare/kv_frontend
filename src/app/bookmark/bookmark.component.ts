import {Component, Input} from '@angular/core';
import {Bookmark} from "./bookmark";
import {MdDialog} from "@angular/material";
import {EditBookmarkDialogComponent} from "./edit-bookmark-dialog.component";
import {BookmarkService} from "./bookmark.service";

@Component({
  selector: 'kvfe-bookmark',
  templateUrl: './bookmark.component.html',
  styleUrls: ['./bookmark.component.css']
})
export class BookmarkComponent {

  @Input() bookmark: Bookmark;

  constructor(public dialog: MdDialog, public bookmarkService: BookmarkService) {
  }


  styleFolders(ancestors: string[]) {
    let s: string[] = [];

    for (let dir of ancestors) {
      if (dir.length > 1) {
        s.push(dir)
      }
    }
    return "/" + s.join('/');
  }

  urlDomain() {
    let a = document.createElement('a');
    a.href = this.bookmark.url;
    return a.protocol + "//" + a.hostname + "/favicon.ico";
  }

  // onImageLoadFails(image_tag){
  //   image_tag.src = "../../assets/images/ic_public_black_24dp_2x.png";
  // }
  onEdit(event) {
    event.preventDefault();
    event.stopPropagation();
    let dialogRef = this.dialog.open(EditBookmarkDialogComponent, {
      data: this.bookmark,
      // height:'400',
      width: '400',
    });

    dialogRef.afterClosed().subscribe(
      result => {
        if (result != null) {
          // console.debug(result);
          this.bookmarkService.updateBookmark(result);
        }
        // else console.debug('null');
      }
    );
  }

}
