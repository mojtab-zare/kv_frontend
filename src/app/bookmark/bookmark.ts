export class Bookmark {

  constructor(public _id: string,
              public ancestors: string[],
              public title: string,
              public url: string,
              public dateAdded: Date) {
  }
}
