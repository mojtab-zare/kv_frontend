import {Component, OnInit} from '@angular/core';
import {Bookmark} from "./bookmark";
import {BookmarkService} from "./bookmark.service";

@Component({
  selector: 'kvfe-bookmark-list',
  templateUrl: './bookmark-list.component.html',
  styleUrls: ['./bookmark-list.component.css']
})
export class BookmarkListComponent implements OnInit {

  constructor(private bookmarkService: BookmarkService) {
  }

  public bookmarks: Bookmark[];

  ngOnInit() {
    this.bookmarkService.bookmarksChanged.subscribe(
      (bookmarks: Bookmark[]) => this.bookmarks = bookmarks
    );
    this.bookmarkService.getBookmarks();
  }

}
